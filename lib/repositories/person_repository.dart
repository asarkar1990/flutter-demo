import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tet/models/person.dart';

var fs = Firestore.instance;

class PersonRepository {
  final CollectionReference _db = fs.collection('persons');

//  firestore collection ref
// save/ update

  Future<bool> save(Person _p) async {
    DocumentReference doc;
    bool saved;
//    check if doc is a existing document
    if (_p.uid == null) {
      doc = _db.document();
    } else {
      doc = _db.document(_p.uid);
    }
    await doc
        .setData(_p.toMap(), merge: true)
        .then((value) => saved = true)
        .catchError((value) => saved = false);
    return saved;
  }

// read single

// read list
  Future<List<Person>> persons() async {
    List<Person> _list = [];
    QuerySnapshot snapshots = await _db.getDocuments();

    /// using for-each
//    snapshots.documents.forEach((document) {
//      var person = Person.fromFirestore(document);
//      _list.add(person);
//    });

    /// using for loop
//    for (DocumentSnapshot item in snapshots.documents) {
//      var person = Person.fromFirestore(item);
//      _list.add(person);
//    }

//    return _list;

    /// using map
    return snapshots.documents
        .map((document) => Person.fromFirestore(document))
        .toList();
  }

// delete

}
