import 'package:cloud_firestore/cloud_firestore.dart';

class Person {
//  Properties
  String uid;
  String name;
  int age;
  Timestamp dob;
  String gender;
  String state;

  Person({this.uid, this.name, this.age, this.dob, this.gender, this.state});

//  deserialization
  factory Person.fromMap(map) {
    var uid = map['uid'];
    var name = map['name'];
    var age = map['age'];
    var dob = map['dob'];
    var gender = map['gender'];
    var state = map['state'];
    var object = Person(
        uid: uid, name: name, age: age, dob: dob, gender: gender, state: state);
    return object;
  }

  factory Person.fromFirestore(DocumentSnapshot snapshot) =>
      Person.fromMap(snapshot.data);

  //  serialization
  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};
    map['uid'] = this.uid;
    map['name'] = this.name;
    map['age'] = this.age;
    map['dob'] = this.dob;
    map['gender'] = this.gender;
    map['state'] = this.state;
    return map;
  }
}
