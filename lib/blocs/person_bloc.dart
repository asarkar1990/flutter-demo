import 'package:tet/models/person.dart';
import 'package:tet/repositories/person_repository.dart';

class PersonBloc {
  var _repo = PersonRepository();

  Future<bool> save(Person _p) => _repo.save(_p);

  Future<List<Person>> persons() => _repo.persons();
}
