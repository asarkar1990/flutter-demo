import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tet/blocs/person_bloc.dart';
import 'package:tet/models/person.dart';
import 'package:tet/pages/person_add_edit_screen.dart';

class PersonListScreen extends StatelessWidget {
  var _bloc = PersonBloc();

  @override
  Widget build(BuildContext context) => Scaffold(
//        appBar: AppBar(
//          title: Text('Person List'),
//        ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => PersonAddEditScreen()));
          },
          child: Icon(Icons.add)),
      body: FutureBuilder<List<Person>>(
        future: _bloc.persons(),
        builder: (BuildContext context, AsyncSnapshot<List<Person>> snapshot) =>
            snapshot.hasData
                ? ListView.separated(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) =>
                        buildItem(context, snapshot.data[index]),
                    separatorBuilder: (BuildContext context, int index) =>
                        Divider(
                      color: Colors.grey,
                    ),
                  )
                : Center(
                    child: CircularProgressIndicator(),
                  ),
      ));

  buildItem(BuildContext context, Person person) => ListTile(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (BuildContext context) =>
                      PersonAddEditScreen(person: person)));
        },
        leading: Icon(Icons.person),
        title: Text('${person.name}'),
        subtitle: Text('Person'),
        trailing: Icon(Icons.play_arrow),
      );
}
