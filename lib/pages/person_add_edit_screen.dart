import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart';
import 'package:tet/blocs/person_bloc.dart';
import 'package:tet/models/person.dart';

class PersonAddEditScreen extends StatefulWidget {
  Person person;

  PersonAddEditScreen({this.person});

  @override
  _PersonAddEditScreenState createState() => _PersonAddEditScreenState();
}

class _PersonAddEditScreenState extends State<PersonAddEditScreen> {
  Person _person;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _person = widget.person ?? Person();
  }

  var _bloc = PersonBloc();

  String _gender;

  final _formKey = GlobalKey<FormState>();

  var _nameController = TextEditingController();

  var _ageController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add/ Edit'),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  initialValue: _person.name == null ? '' : _person.name,
                  decoration: InputDecoration(labelText: "Name"),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please write your name";
                    } else
                      return null;
                  },
                ),
                TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(labelText: "Age"),
                    initialValue: _person.age.toString(),
                    onSaved: (value) => _person.age = int.parse(value)),
                DateTimeField(
                  initialValue: _person.dob == null
                      ? DateTime.now()
                      : _person.dob.toDate(),
                  decoration: InputDecoration(labelText: "DOB"),
                  format: DateFormat("dd-mm-yyyy"),
                  onShowPicker: (BuildContext context, DateTime currentValue) {
                    return showDatePicker(
                        context: context,
                        firstDate: DateTime(1900),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                  },
                  onChanged: (value) => _person.dob = Timestamp.fromDate(value),
                ),
                Row(
                  children: <Widget>[
                    Text('Male'),
                    Radio(
                        value: "Male",
                        groupValue: _person.gender,
                        onChanged: (value) {
                          setState(() {
                            _person.gender = value;
                          });
                        }),
                    Text('Female'),
                    Radio(
                        value: "Female",
                        groupValue: _person.gender,
                        onChanged: (value) {
                          setState(() {
                            _person.gender = value;
                          });
                        }),
                  ],
                ),
                DropdownButtonFormField<String>(
                  decoration: InputDecoration(labelText: 'State'),
                  value: _person.state,
                  items: buildDropdownItems(),
                  onSaved: (String value) {
//                      permit.district = value;
                  },
                  onChanged: (value) {
//                    setState(() {
                    _person.state = value;
//                    });
                  },
                ),
                RaisedButton(
                  onPressed: () async {
                    var form = _formKey.currentState;
                    if (form.validate()) {
                      form.save();
                      bool saved = await _bloc.save(_person);
                      if (saved) {
                        Navigator.pop(context);
                      }
                    }
                  },
                  child: Text('Save'),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  buildDropdownItems() => [
        DropdownMenuItem(
          value: "Tripura",
          child: Text('Tripura'),
        ),
        DropdownMenuItem(value: "Assam", child: Text('Assam')),
      ];
}
