import 'package:flutter/material.dart';
import 'package:tet/pages/person_list.dart';

main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App',
      home: Scaffold(
          backgroundColor: Colors.red,
          appBar: AppBar(
            centerTitle: true,
            title: Text('Add /Edit Person'),
          ),
          body: PersonListScreen()),
    );
  }
}
